function [ d ] = Hellinger(p, q)
  p1 = p ./ sum(p);
  p2 = q ./ sum(q);
  c = 1 / sqrt(2);
  s = (sqrt(p) - sqrt(q)) .^ 2;
  d = c * sqrt(sum(s));
end
