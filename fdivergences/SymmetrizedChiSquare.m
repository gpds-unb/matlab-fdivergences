function [ d ] = SymmetrizedChiSquare(p, q)
% see: ON A SYMMETRIC DIVERGENCE MEASURE AND INFORMATION INEQUALITIES (5.2)
  p1 = p ./ sum(p);
  p2 = q ./ sum(q);
  d = sum(chifactor(p1, p2));
end

function [ d ] = chifactor(p, q)
  d = (p - q) .* log((p + q) ./ (2 * q));
end
