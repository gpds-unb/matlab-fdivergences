function [ d ] = JensenShannon(p, q)
  m = 0.5 * (p + q);
  d = 0.5 * KullbackLeibler(p, m) + 0.5 * KullbackLeibler(q, m);
end