function [ d ] = KullbackLeibler(p, q)
  p1 = p ./ sum(p);
  p2 = q ./ sum(q);
  d = sum(p1 .* log(p1 ./ p2));
end
