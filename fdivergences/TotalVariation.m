function [ d ] = TotalVariation(p, q)
  p1 = p ./ sum(p);
  p2 = q ./ sum(q);
  d = 0.5 * sum(abs(p1 - p2));
end
