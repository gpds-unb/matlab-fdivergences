function [ d ] = Bhattacharyya(p, q)
  p1 = p ./ sum(p);
  p2 = q ./ sum(q);
  bc = BhattacharyyaCoefficient(p1, p2);
  d = -log(bc);
end

function [ d ] = BhattacharyyaCoefficient(p, q)
  d = sum(sqrt(p .* q));
end