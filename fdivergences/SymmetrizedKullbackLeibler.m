function [ d ] = SymmetrizedKullbackLeibler(p, q)
% see: Johnson, Don, and Sinan Sinanovic. "Symmetrizing the 
% kullback-leibler distance." (2001).
  d = KullbackLeibler(p, q) + KullbackLeibler(q, p);
end
