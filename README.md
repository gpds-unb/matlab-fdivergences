# F-divergences and Distances in Matlab


Small collection of functions to compute f-divergences between two vectors.


License
=======

This project is licensed under [The MIT License](https://opensource.org/licenses/MIT)