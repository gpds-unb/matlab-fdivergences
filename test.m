clc;
clear;

addpath('fdivergences')

% obs: valores negativos geram distâncias complexas

d1 = normrnd(0, 1, [1000 1]);
d2 = normrnd(0, 1, [1000 1]);

KL = KullbackLeibler(d1, d2);
KL2 = SymmetrizedKullbackLeibler(d1, d2);
JS = JensenShannon(d1, d2);
HE = Hellinger(d1, d2);
TV = TotalVariation(d1, d2);
BA = Bhattacharyya(d1, d2);
CHI = SymmetrizedChiSquare(d1, d2);

strg = '%s: %f\n';

fprintf(strg, 'KL', KL);
fprintf(strg, 'KL2', KL2);
fprintf(strg, 'JS', JS);
fprintf(strg, 'HE', HE);
fprintf(strg, 'TV', TV);
fprintf(strg, 'BA', BA);
fprintf(strg, 'CHI', CHI);